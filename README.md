OpenTelemetry configuration for KMC3-XPP.

Feel free to poke around, but this is fairly specialized.

If you're one of us:
 
 - `otelcol-defhost.yaml` is the default [OpenTelemetry Collector](https://opentelemetry.io/docs/collector/)
   configuration that runs on each of the production hosts. It's intended to
   capture logs from `/var/log/services/*.log`, as well as accept
   OTLP telemetry connections from containerized workloads.

   The collector runs in a podman network `otel-network`, uses
   the podman hostname `host-otelcol`, and accepts OTLP on port
   4317.

   Typically, KMC3-XPP has a number of receivers that are specified
   by FQDN in the otelcol YAML file.
